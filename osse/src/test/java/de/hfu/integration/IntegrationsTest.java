package de.hfu.integration;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.*;

public class IntegrationsTest {

    ResidentRepository stub = mock(ResidentRepository.class);
    @BeforeEach
    public void init() {

        ResidentRepositoryStub residents=new ResidentRepositoryStub();

        expect(stub.getResidents()).andReturn(residents.getResidents());
        replay(stub);
    }


    @Test
    public void getFilteredResidentTestSuccess() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("Albert", "" , "", "", null));
        if(residents.size() != 2) fail();
        assertEquals(residents.get(0).getGivenName(), "Albert");
    }

    @Test
    public void getFilteredResidentTestSuccessWildcard() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("A*", "" , "", "", null));
        assertEquals(residents.size(), 2);
    }

    @Test
    public void getFilteredResidentTestNoResults() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        List<Resident> residents = service.getFilteredResidentsList(new Resident("Herbert", "" , "", "", new Date()));
        assertEquals(residents.size(), 0);
    }


                //Unique Tests
    @Test
    public void getUniqueResidentTestSuccess() throws ResidentServiceException {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        Resident resident = service.getUniqueResident(new Resident("Daniel", "" , "", "", null));
        if(resident == null) fail();
        assertEquals(resident.getGivenName(), "Daniel");
    }

    @Test
    public void getUniqueResidentTestFail() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        assertThrows(ResidentServiceException.class, () -> {
            service.getUniqueResident(new Resident("Albert1", "" , "", "", null));
        });
    }

    @Test
    public void getUniqueResidentTestFailWildcard() {
        BaseResidentService service = new BaseResidentService();
        service.setResidentRepository(stub);
        assertThrows(ResidentServiceException.class, () -> {
            service.getUniqueResident(new Resident("Al*", "" , "", "", null));
        });
    }

    private List<Resident> getResidentsMock() {
        Resident resident1 = new Resident("Albert", "Aachener", "sdasdd 7", "Furtwangen", new Date(2000,02,23) );
        Resident resident2 = new Resident("Bob", "Boberd", "sdasdd 7", "Furtwangen", new Date(2000,05,8) );
        Resident resident3 = new Resident("Cedrik", "Chillig", "sdasdd 7", "Furtwangen", new Date(2001,11,18) );
        Resident resident4 = new Resident("Daniel", "Deppert", "sdasdd 7", "Furtwangen", new Date(2003,01,27) );
        Resident resident5 = new Resident("Boberd", "Müller", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
        Resident resident6 = new Resident("Vladimir", "Herbert", "sdasdd 7", "Furtwangen", new Date(2002,06,4) );
        return Arrays.asList(resident1, resident2, resident3, resident4, resident5, resident6);
    }
}
