package de.hfu;
import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main( String[] args ) {
        Scanner scanner = new Scanner(System.in);
        String ausgabe = scanner.nextLine();

        System.out.println(ausgabe.toUpperCase());
    }
}
